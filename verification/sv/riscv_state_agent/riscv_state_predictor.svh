//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

// -----------------------------------------------------------------------------------------------
// riscv_state_predictor
// -----------------------------------------------------------------------------------------------
class riscv_state_predictor extends uvm_monitor;

	// uvm factory registation
	`uvm_component_utils_begin(riscv_state_predictor)
	`uvm_component_utils_end

  // data members

	// component members
  chandle s;
  virtual riscv_state_if state_if;
  uvm_analysis_port #(riscv_state_seq_item) spike_state_port; 
	// uvm methods
	extern function new(string name="riscv_state_predictor", uvm_component parent);
	extern virtual function void build_phase(uvm_phase phase);
	extern virtual function void connect_phase(uvm_phase phase);
	extern virtual task run_phase(uvm_phase phase);
endclass : riscv_state_predictor

// -----------------------------------------------------------------------------------------------
// new
// -----------------------------------------------------------------------------------------------
function riscv_state_predictor::new(string name="riscv_state_predictor", uvm_component parent);
	super.new(name,parent);
  s = SpawnSpikeIF("/scratch/lavanya/c-class/verification/workdir/directed/riscv-tests/isa/rv64ui/p/add/add.elf");
endfunction : new

// -----------------------------------------------------------------------------------------------
// build
// -----------------------------------------------------------------------------------------------
function void riscv_state_predictor::build_phase(uvm_phase phase);
	super.build_phase(phase);
  spike_state_port = new("spike_state_port", this);
  if (!uvm_config_db #(virtual riscv_state_if)::get(uvm_root::get(),"*", "state_if",state_if)) begin 
    `uvm_fatal(get_full_name(), $sformatf("Failed to get inst ")) 
  end 

endfunction : build_phase

// -----------------------------------------------------------------------------------------------
// connect
// -----------------------------------------------------------------------------------------------
function void riscv_state_predictor::connect_phase(uvm_phase phase);
  super.connect_phase(phase);
endfunction : connect_phase

// -----------------------------------------------------------------------------------------------
// run
// -----------------------------------------------------------------------------------------------
task riscv_state_predictor::run_phase(uvm_phase phase);
  
  riscv_state_seq_item spike_pkt;

  forever @(posedge state_if.clk) begin
    //`uvm_info("MYINFO1", $sformatf("instr retire: %0h",state_if.instr_retire), UVM_LOW)
    //`uvm_info("MYINFO1", $sformatf("x5: %0h pc:%0h memdata:%h%h ",state_if.x5, state_if.pc, state_if.data_msb,state_if.data_lsb), UVM_LOW)
    if (state_if.instr_retire == 'b1) begin
      longint pc;
      logic [31:0] temp;
      logic [31:0] addr;
      logic [63:0] reg_value;

      spike_pkt = riscv_state_seq_item::type_id::create("spike_pkt",this); 
      for(temp='h0;temp<'h20;temp++) begin
        addr = `SLSV_XPR_BASE + temp;
        reg_value = CgetVariable(s,addr);
        spike_pkt.int_regs.push_back(reg_value);
      end
      addr= `SLSV_XPR_BASE + 'h20;
      pc = CgetVariable(s,addr);
      spike_pkt.pc = pc;
      for(temp='h0; temp<spike_pkt.csr_index.size; temp++) begin
        logic [31:0] index;
        index = spike_pkt.csr_index[temp];
        spike_pkt.csr[index]=CgetVariable(s,index);  
        //`uvm_info("SLSV", $sformatf("-----csr[%h]=%h------------", index,CgetVariable(s,index)), UVM_LOW)
      end
      addr = `CSR_MSTATUS;
//      spike_pkt.csr_mstatus= CgetVariable(s,addr);
//      `uvm_info("SLSV", $sformatf("-----------------"), UVM_LOW)
  //    `uvm_info("SLSV", $sformatf("-----MSTATUS=%h------------", spike_pkt.csr_mstatus), UVM_LOW)
      spike_state_port.write(spike_pkt);
//      spike_pkt.print_riscv_state();
      CsingleStep(s);
    end
  end
endtask : run_phase

