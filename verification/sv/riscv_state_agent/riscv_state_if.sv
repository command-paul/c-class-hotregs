//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

interface riscv_state_if(input clk,
                         input reset
                       );

  logic[63:0] x0;                 
  logic[63:0] x1;                 
  logic[63:0] x2;                 
  logic[63:0] x3;                 
  logic[63:0] x4;                 
  logic[63:0] x5;                 
  logic[63:0] x6;                 
  logic[63:0] x7;                 
  logic[63:0] x8;                 
  logic[63:0] x9;                 
  logic[63:0] x10;                 
  logic[63:0] x11;                 
  logic[63:0] x12;                 
  logic[63:0] x13;                 
  logic[63:0] x14;                 
  logic[63:0] x15;                 
  logic[63:0] x16;                 
  logic[63:0] x17;                 
  logic[63:0] x18;                 
  logic[63:0] x19;                 
  logic[63:0] x20;                 
  logic[63:0] x21;                 
  logic[63:0] x22;                 
  logic[63:0] x23;                 
  logic[63:0] x24;                 
  logic[63:0] x25;                 
  logic[63:0] x26;                 
  logic[63:0] x27;                 
  logic[63:0] x28;                 
  logic[63:0] x29;                 
  logic[63:0] x30;                 
  logic[63:0] x31;                 

  logic [63:0] csr_cycle;
  logic [63:0] csr_time;
  logic [63:0] csr_instret;
  logic [63:0] csr_sstatus;
  logic [63:0] csr_sie;
  logic [63:0] csr_stvec;
  logic [63:0] csr_scounteren;
  logic [63:0] csr_sscratch;
  logic [63:0] csr_sepc;
  logic [63:0] csr_scause;
  logic [63:0] csr_stval;
  logic [63:0] csr_sip;
  logic [63:0] csr_satp;
  logic [63:0] csr_mstatus;
  logic [63:0] csr_misa;
  logic [63:0] csr_medeleg;
  logic [63:0] csr_mideleg;
  logic [63:0] csr_mie;
  logic [63:0] csr_mtvec;
  logic [63:0] csr_mcounteren;
  logic [63:0] csr_mscratch;
  logic [63:0] csr_mepc;
  logic [63:0] csr_mcause;
  logic [63:0] csr_mtval;
  logic [63:0] csr_mip;
  logic [63:0] csr_tselect;
  logic [63:0] csr_tdata1;
  logic [63:0] csr_tdata2;
  logic [63:0] csr_tdata3;
  logic [63:0] csr_dcsr;
  logic [63:0] csr_dpc;
  logic [63:0] csr_dscratch;
  logic [63:0] csr_mcycle;
  logic [63:0] csr_minstret;
  logic [63:0] csr_mvendorid;
  logic [63:0] csr_marchid;
  logic [63:0] csr_mimpid;
  logic [63:0] csr_mhartid;
  logic [63:0] csr_cycleh;
  logic [63:0] csr_timeh;
  logic [63:0] csr_instreth;
  logic [63:0] csr_mcycleh;
  logic [63:0] csr_minstreth;

  logic[63:0] pc;
  logic[31:0] data_msb;
  logic[31:0] data_lsb;
  logic instr_retire;
  logic rst_n;
//  bit setReset;
//
//  always @(posedge clk) begin
//    if(setReset == 1) begin
//      force tb.RST_N = 1;
//    end
//    else begin
//      release tb.RST_N;
//    end
//  end
endinterface : riscv_state_if
