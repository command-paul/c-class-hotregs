
module top_tb;
  import "DPI-C" context function chandle SpawnSpikeIF(input string);
  import "DPI-C" context function longint CgetVariable(input chandle, input int);
  import "DPI-C" context function int CsingleStep(input chandle);
  //import "DPI-C" context function DestroySpikeIf(input int);

reg clk, rst_n;

state_if iState(clk,rst_n);

mkTbSoc tb(.CLK(clk),
                  .RST_N(rst_n)
                );

                
initial begin
  clk=0;
  forever #10 clk = ~clk;
end

task do_run;
  chandle s;
  s = SpawnSpikeIF("/scratch/lavanya/c-class/verification/workdir/directed/riscv-tests/isa/rv64ui/p/add/add.elf");
  forever @(posedge clk) begin
    if (iState.write_back == 'b1) begin
      longint pc;
    logic[31:0] addr;
    logic [63:0] x0; 
    logic [63:0] x1 ;
    logic [63:0] x2 ;
    logic [63:0] x3 ;
    logic [63:0] x4 ;
    logic [63:0] x5 ;
    logic [63:0] x6 ;
    logic [63:0] x7 ;
    logic [63:0] x8 ;
    logic [63:0] x9 ;
    logic [63:0] x10;
    logic [63:0] x11;
    logic [63:0] x12;
    logic [63:0] x13;
    logic [63:0] x14;
    logic [63:0] x15;
    logic [63:0] x16;
    logic [63:0] x17;
    logic [63:0] x18;
    logic [63:0] x19;
    logic [63:0] x20;
    logic [63:0] x21;
    logic [63:0] x22;
    logic [63:0] x23;
    logic [63:0] x24;
    logic [63:0] x25;
    logic [63:0] x26;
    logic [63:0] x27;
    logic [63:0] x28;
    logic [63:0] x29;
    logic [63:0] x30;
    logic [63:0] x31;
    logic [31:0] temp;

    for(temp='h0;temp <'h20; temp++) begin
      addr = 'h1000 + temp;
      $display("x[%0d]=0x%h",temp, CgetVariable(s,addr));
    end
      addr='h1000;  x0 = CgetVariable(s,addr);
      addr= 'h1001; x1 = CgetVariable(s,addr);
      addr= 'h1002; x2 = CgetVariable(s,addr);
      addr= 'h1003; x3 = CgetVariable(s,addr);
      addr= 'h1004; x4 = CgetVariable(s,addr);
      addr= 'h1005; x5 = CgetVariable(s,addr);
      addr= 'h1006; x6 = CgetVariable(s,addr);
      addr= 'h1007; x7 = CgetVariable(s,addr);
      addr= 'h1008; x8 = CgetVariable(s,addr);
      addr= 'h1009; x9 = CgetVariable(s,addr);
      addr= 'h100a; x10 = CgetVariable(s,addr);
      addr= 'h100b; x11 = CgetVariable(s,addr);
      addr= 'h100c; x12 = CgetVariable(s,addr);
      addr= 'h100d; x13 = CgetVariable(s,addr);
      addr= 'h100e; x14 = CgetVariable(s,addr);
      addr= 'h100f; x15 = CgetVariable(s,addr);
      addr= 'h1010; x16 = CgetVariable(s,addr);
      addr= 'h1011; x17 = CgetVariable(s,addr);
      addr= 'h1012; x18 = CgetVariable(s,addr);
      addr= 'h1013; x19 = CgetVariable(s,addr);
      addr= 'h1014; x20 = CgetVariable(s,addr);
      addr= 'h1015; x21 = CgetVariable(s,addr);
      addr= 'h1016; x22 = CgetVariable(s,addr);
      addr= 'h1017; x23 = CgetVariable(s,addr);
      addr= 'h1018; x24 = CgetVariable(s,addr);
      addr= 'h1019; x25 = CgetVariable(s,addr);
      addr= 'h101a; x26 = CgetVariable(s,addr);
      addr= 'h101b; x27 = CgetVariable(s,addr);
      addr= 'h101c; x28 = CgetVariable(s,addr);
      addr= 'h101d; x29 = CgetVariable(s,addr);
      addr= 'h101e; x30 = CgetVariable(s,addr);
      addr= 'h101f; x31 = CgetVariable(s,addr);
      addr = 'h1020; pc = CgetVariable(s,addr);

      $display("[pc]=0x%h",pc);
      //$display("shakti.pc=%h x5=%h  x10=%h x11=%h",iState.pc_value, iState.x5_value, iState.x10_value,iState.x11_value);
      $display("[zero|x0]: 0x%h  [ra|x1]: 0x%h  [sp |x2]: 0x%h  [gp |x3]: 0x%h",x0, x1, x2, x3);
      $display("[tp  |x4]: 0x%h  [t0|x5]: 0x%h  [t1 |x6]: 0x%h  [t2 |x7]: 0x%h",x4, x5, x6, x7);
      $display("[s0  |x8]: 0x%h  [s1|x9]: 0x%h  [a0 |x10]: 0x%h  [a1 |x11]: 0x%h",x8, x9, x10, x11);
      $display("[a2  |x12]: 0x%h  [a3|x13]: 0x%h  [a4 |x14]: 0x%h  [a5 |x15]: 0x%h",x12, x13, x14, x15);
      $display("[a6  |x16]: 0x%h  [a7|x17]: 0x%h  [s2 |x18]: 0x%h  [s3 |x19]: 0x%h",x16, x17, x18, x19);
      $display("[s4  |x20]: 0x%h  [s5|x21]: 0x%h  [s6 |x22]: 0x%h  [s7 |x23]: 0x%h",x20, x21, x22, x23) ;
      $display("[s8  |x24]: 0x%h  [s9|x25]: 0x%h  [s10|x26]: 0x%h  [s11|x27]: 0x%h",x24, x25, x26, x27) ;
      $display("[t3  |x28]: 0x%h  [t4|x29]: 0x%h  [t5 |x30]: 0x%h  [t6 |x31]: 0x%h",x28, x29, x30, x31) ;

      CsingleStep(s);
    end
    //$display("\n code=%h %h",tb.soc.main_memory_dmemMSB.RAM[iState.pc_value],tb.soc.main_memory_dmemLSB.RAM[pc]);
    //$display("\n boot=%h %h ",tb.soc.bootrom.dmemMSB.RAM[pc], tb.soc.bootrom.dmemLSB.RAM[pc]);
  end
endtask

initial begin
  rst_n = 0;
  $readmemh("boot.LSB", tb.soc.bootrom.dmemLSB.RAM);
  $readmemh("boot.MSB", tb.soc.bootrom.dmemMSB.RAM);
  $readmemh("code.mem.LSB", tb.soc.main_memory_dmemLSB.RAM);
  $readmemh("code.mem.MSB", tb.soc.main_memory_dmemMSB.RAM);
  #100 rst_n = 1;
  do_run();  
  



  //$display("\n pc=%h",iState.pc_value);
  //#50; 
  //$display("\n pc=%h",iState.pc_value);
  //$display("\n x0=%h",iState.x0_value);
  //$display("\n x1=%h",iState.x1_value);
  //$display("\n x2=%h",iState.x2_value);
  //$display("\n x3=%h",iState.x3_value);
  //$display("\n x4=%h",iState.x4_value);
  //$display("\n x5=%h",iState.x5_value);
  //$display("\n x6=%h",iState.x6_value);
  //#55; 
  //$display("\n x0=%h",iState.x0_value);
  //$display("\n x1=%h",iState.x1_value);
  //$display("\n x2=%h",iState.x2_value);
  //$display("\n x3=%h",iState.x3_value);
  //$display("\n x4=%h",iState.x4_value);
  //$display("\n x5=%h",iState.x5_value);
  //$display("\n x6=%h",iState.x6_value);
  //$display("\n pc=%h",iState.pc_value);
  //#55; 
  //$display("\n x0=%h",iState.x0_value);
  //$display("\n x21=%h",iState.x21_value);
  //$display("\n x22=%h",iState.x22_value);
  //$display("\n pc=%h",iState.pc_value);
  //$display("\n pc=%h",iState.pc_value);
  //$display("\n x0=%h",iState.x0_value);
  //$display("\n x31=%h",iState.x31_value);
  //$display("\n x12=%h",iState.x12_value);
  //$display("\n code=%h %h",tb.soc.main_memory_dmemMSB.RAM[2],tb.soc.main_memory_dmemLSB.RAM[2]);
  //$display("\n boot=%h %h ",tb.soc.bootrom.dmemMSB.RAM[2], tb.soc.bootrom.dmemLSB.RAM[2]);
  #5500000; 
  //DestroySpikeIf(s);
  $finish;
end


assign iState.pc_value =  tb.soc.core.riscv.rx_w_data$wget[146:108];
assign iState.write_back = tb.soc.core.riscv.WILL_FIRE_RL_rl_write_back;
assign iState.x0_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[0];
assign iState.x1_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[1];
assign iState.x2_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[2];
assign iState.x3_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[3];
assign iState.x4_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[4];
assign iState.x5_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[5];
assign iState.x6_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[6];
assign iState.x7_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[7];
assign iState.x8_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[8];
assign iState.x9_value =  tb.soc.core.riscv.decode.registerfile.integer_rf.arr[9];
assign iState.x10_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[10];
assign iState.x11_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[11];
assign iState.x12_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[12];
assign iState.x13_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[13];
assign iState.x14_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[14];
assign iState.x15_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[15];
assign iState.x16_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[16];
assign iState.x17_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[17];
assign iState.x18_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[18];
assign iState.x19_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[19];
assign iState.x20_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[20];
assign iState.x21_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[21];
assign iState.x22_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[22];
assign iState.x23_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[23];
assign iState.x24_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[24];
assign iState.x25_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[25];
assign iState.x26_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[26];
assign iState.x27_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[27];
assign iState.x28_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[28];
assign iState.x29_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[29];
assign iState.x30_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[30];
assign iState.x31_value = tb.soc.core.riscv.decode.registerfile.integer_rf.arr[31];


//mkriscv riscv();
//axi_if iAXI(CLK,RESET);


//  mkriscv riscv(.reset_vector(39'h0000001000),
//		.CLK(clk),
//		.RST_N(rst_n),
//		.boot_sequence_bootseq('b1),
//		.clint_msip_intrpt('h0),
//		.clint_mtime_c_mtime('h0),
//		.clint_mtip_intrpt('h0),
//		.instruction_response_from_imem_x(riscv$instruction_response_from_imem_x),
//		.prediction_response_put(riscv$prediction_response_put),
//		.response_from_dmem_put(riscv$response_from_dmem_put),
//		.set_external_interrupt_i(riscv$set_external_interrupt_i),
//		.EN_request_to_imem_get(1'b1),
//		.EN_instruction_response_from_imem(1'b1),
//		.EN_prediction_response_put(1'b1),
//		.EN_send_prediction_request_get(1'b1),
//		.EN_request_to_dmem_get(1'b1),
//		.EN_response_from_dmem_put(1'b1),
//		.EN_set_external_interrupt(1'b1),
//		.EN_clint_msip(1'b1),
//		.EN_clint_mtip(1'b1),
//		.EN_clint_mtime(1'b1),
//		.request_to_imem_get(riscv$request_to_imem_get),
//		.RDY_request_to_imem_get(riscv$RDY_request_to_imem_get),
//		.RDY_instruction_response_from_imem(riscv$RDY_instruction_response_from_imem),
//		.RDY_prediction_response_put(riscv$RDY_prediction_response_put),
//		.send_prediction_request_get(riscv$send_prediction_request_get),
//		.RDY_send_prediction_request_get(),
//		.training_data(riscv$training_data),
//		.RDY_training_data(),
//		.request_to_dmem_get(riscv$request_to_dmem_get),
//		.RDY_request_to_dmem_get(riscv$RDY_request_to_dmem_get),
//		.RDY_response_from_dmem_put(),
//		.flush_dmem(riscv$flush_dmem),
//		.RDY_flush_dmem(),
//		.RDY_set_external_interrupt(riscv$RDY_set_external_interrupt),
//		.send_satp(riscv$send_satp),
//		.RDY_send_satp(),
//		.perm_to_TLB(riscv$perm_to_TLB),
//		.RDY_perm_to_TLB(),
//		.mmu_cache_disable(riscv$mmu_cache_disable),
//		.RDY_mmu_cache_disable(),
//		.fence_tlbs(riscv$fence_tlbs),
//		.RDY_fence_tlbs(riscv$RDY_fence_tlbs),
//		.RDY_clint_msip(),
//		.RDY_clint_mtip(),
//		.RDY_clint_mtime());


endmodule
