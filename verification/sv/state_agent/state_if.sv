//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

interface state_if(input CLK,
                   input RESET);

  logic[63:0] x0_value;                 
  logic[63:0] x1_value;                 
  logic[63:0] x2_value;                 
  logic[63:0] x3_value;                 
  logic[63:0] x4_value;                 
  logic[63:0] x5_value;                 
  logic[63:0] x6_value;                 
  logic[63:0] x7_value;                 
  logic[63:0] x8_value;                 
  logic[63:0] x9_value;                 
  logic[63:0] x10_value;                 
  logic[63:0] x11_value;                 
  logic[63:0] x12_value;                 
  logic[63:0] x13_value;                 
  logic[63:0] x14_value;                 
  logic[63:0] x15_value;                 
  logic[63:0] x16_value;                 
  logic[63:0] x17_value;                 
  logic[63:0] x18_value;                 
  logic[63:0] x19_value;                 
  logic[63:0] x20_value;                 
  logic[63:0] x21_value;                 
  logic[63:0] x22_value;                 
  logic[63:0] x23_value;                 
  logic[63:0] x24_value;                 
  logic[63:0] x25_value;                 
  logic[63:0] x26_value;                 
  logic[63:0] x27_value;                 
  logic[63:0] x28_value;                 
  logic[63:0] x29_value;                 
  logic[63:0] x30_value;                 
  logic[63:0] x31_value;                 

  logic[63:0] pc_value;

  logic write_back;
endinterface
